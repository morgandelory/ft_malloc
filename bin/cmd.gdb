set exec-wrapper env LD_LIBRARY_PATH=. LD_PRELOAD=libft_malloc.so
file dummy

b breakit

define PRINT_HEAP
    printf "|  %-18p|%18p|%18p|%18d|\n", $arg0, $arg0->prev, $arg0->next, $arg0->size
end

define PRINT_BLOCK
    printf "|%3d|%18p|%18p|%18p|%12d|%u|%#2x|\n", $arg1, $arg0, $arg0->prev, $arg0->next, $arg0->size, $arg0->free, *((void *)$arg0 + sizeof(t_block))
end

define HP
  set var $h = g_heap[$arg0]
  set var $i = 1
  while $h
    printf "| Heap #%5d |\n", $i
    PRINT_HEAP $h
    BL $h
    set var $h = $h->next
    set var $i = $i + 1
  end
end

define BL
  set var $b = (t_block *)($arg0 + 1)
  set var $i = 1
  while ($b != 0)
    PRINT_BLOCK $b $i
    set var $b = $b->next
    set var $i = $i + 1
  end
end
