# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    test.sh                                            :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mdelory <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2021/01/04 14:07:16 by mdelory           #+#    #+#              #
#    Updated: 2021/01/04 14:17:31 by mdelory          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#!/bin/sh

HOSTTYPE=`uname -m`_`uname -s`

export DYLD_LIBRARY_PATH=.
export DYLD_INSERT_LIBRARIES=libft_malloc_$HOSTTYPE.so
export DYLD_FORCE_FLAT_NAMESPACE=1

export LD_LIBRARY_PATH=.
export LD_PRELOAD=libft_malloc.so

$@
