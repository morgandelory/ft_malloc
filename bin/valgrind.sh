# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    test.sh                                            :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mdelory <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2021/01/04 14:07:16 by mdelory           #+#    #+#              #
#    Updated: 2021/01/04 14:17:31 by mdelory          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#!/bin/sh

export DYLD_LIBRARY_PATH=.
export DYLD_INSERT_LIBRARIES=libft_malloc.so
export DYLD_FORCE_FLAT_NAMESPACE=1

export LD_LIBRARY_PATH=.
export LD_PRELOAD=libft_malloc.so

valgrind -v --trace-children=yes env LD_LIBRARY_PATH=$DYLD_LIBRARY_PATH LD_INSERT_LIBRARIES=$DYLD_INSERT_LIBRARIES $@
