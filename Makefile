# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mdelory <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2021/01/04 12:37:45 by mdelory           #+#    #+#              #
#    Updated: 2021/01/05 16:43:02 by mdelory          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

ifeq ($(HOSTTYPE),)
	HOSTTYPE := $(shell uname -m)_$(shell uname -s)
endif

# FILES #
PATH_SRC = src
PATH_OBJ = obj
PATH_LIB = libft

SOURCES += calloc.c free.c malloc.c realloc.c \
		   heap.c debug.c utils.c

OBJECTS = $(SOURCES:%.c=$(PATH_OBJ)/%.o)

# VARIABLES #

NAME = libft_malloc_$(HOSTTYPE).so
LIB_NAME = libft_malloc.so

CC = gcc

FLAGS_CC = -g -Wall -Wextra -Werror
FLAGS_LIB = -Iinc -Ilibft/includes -Llibft -lft

# COMMANDS #
.PHONY: all libft dummy clean fclean re

all: $(NAME)

$(NAME): libft $(OBJECTS)
	$(CC) $(FLAGS_CC) $(FLAGS_LIB) -Wl,--whole-archive libft/libft.a -Wl,--no-whole-archive -o $@ -shared $(OBJECTS)
	@rm -f $(LIB_NAME)
	ln -s $(NAME) $(LIB_NAME)

$(PATH_OBJ)/%.o: $(PATH_SRC)/%.c
	@mkdir -p $(@D)
	$(CC) -c -o $@ $(FLAGS_CC) $(FLAGS_LIB) -fPIC $^

libft:
	$(MAKE) -C libft

dummy: $(NAME)
	$(CC) -g dummy.c -Iinc -Ilibft/includes -L. -lft_malloc -o dummy

clean:
	@rm -rf $(PATH_OBJ)

fclean: clean
	@rm -f $(NAME) $(LIB_NAME)

re: fclean $(NAME)
