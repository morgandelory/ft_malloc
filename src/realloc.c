#include "malloc.h"

int                 resize_block(t_block *block, size_t size)
{
    t_block         *leftover;

    if (get_heap_type_from_size(block->size) != get_heap_type_from_size(size))
        return 0;
    if (align(size) == align(block->size))
        return 1;
    if (block->next && block->next->free && \
        align(block->size) + align(block->next->size) + sizeof(t_block) >= size)
    {
        leftover = split_block(block->next, align(size) - align(block->size) - sizeof(t_block));
        merge_blocks(block, block->next);
        block->next = leftover;
        return 1;
    }
    return 0;
}

void                *realloc(void *ptr, size_t size)
{
    size_t          old_size;
    void            *new;
    t_block         *block;

    size = (size + 15) & ~15;
    if (ptr == NULL)
        return malloc(size);
    block = ptr - sizeof(t_block);
    old_size = block->size;
    if (resize_block(block, size))
        return ptr;
    if ((new = malloc(size)))
        ft_memcpy(new, ptr, old_size < size ? old_size : size);
    free(ptr);
    return new;
}