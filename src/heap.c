/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   heap.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdelory <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/04 14:30:32 by mdelory           #+#    #+#             */
/*   Updated: 2021/01/05 17:38:44 by mdelory          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void            merge_blocks(t_block *dst, t_block *src)
{
    dst->size += src->size + sizeof(t_block);
    dst->next = src->next;
    if (src->next)
        src->next->prev = dst;
}

t_heap_type		get_heap_type_from_size(size_t size)
{
    size_t      aligned_size;

    aligned_size = align(size);
	if (aligned_size <= TINY_BLOCK_SIZE)
		return TINY;
	else if (aligned_size <= SMALL_BLOCK_SIZE)
		return SMALL;
	return LARGE;
}

void			*alloc_block(t_heap *heap, size_t size)
{
	t_block		*block;

    block = HEAP_CONTENT(heap);
	while (block && (align(size) > align(block->size) || block->free == 0))
        block = block->next;
    if (!block)
	    return NULL;
    if (align(size) - align(block->size) >= sizeof(t_block) + TINY_BLOCK_SIZE)
	    split_block(block, size);
    block->free = 0;
    block->size = size;
	return BLOCK_CONTENT(block);
}

t_block         *split_block(t_block *block, size_t size)
{
	t_block		*leftover;
    size_t      aligned_size;

    aligned_size = align(size);
    leftover = BLOCK_CONTENT(block) + aligned_size;
    if (leftover != block->next && align(block->size) - aligned_size >= sizeof(t_block))
    {
        leftover->size = block->size - aligned_size - sizeof(t_block);
        leftover->free = 1;
        leftover->next = block->next;
        block->size = size;
        block->next = leftover;
        if (block->next)
            block->next->prev = leftover;
    }
    leftover->prev = block;
	return leftover;
}

t_heap			*create_heap(size_t size)
{
	t_heap		*heap;
	t_block		*first_block;

	heap = (t_heap *)mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANON, -1, 0);
	heap->prev = NULL;
	heap->next = NULL;
	heap->size = size - sizeof(t_heap);
    first_block = HEAP_CONTENT(heap);
    first_block->prev = NULL;
    first_block->next = NULL;
    first_block->size = heap->size - sizeof(t_block);
    first_block->free = 1;
	return heap;
}

void			destroy_heap(t_heap *heap, t_heap_type type)
{
    if (type != LARGE && g_heap[type] == heap && !heap->next)
        return;
    if (heap->next)
        heap->next->prev = heap->prev;
    if (heap->prev)
        heap->prev->next = heap->next;
    else
        g_heap[type] = heap->next;
    munmap(heap, heap->size + sizeof(t_heap));
}

size_t          calc_heap_size(size_t size)
{
    t_heap_type type;

    size = align(size);
    type = get_heap_type_from_size(size);
    if (type == TINY)
        return TINY_HEAP_ALLOCATION_SIZE;
    if (type == SMALL)
        return SMALL_HEAP_ALLOCATION_SIZE;
    return size + sizeof(t_block) + sizeof(t_heap);
}

void            *alloc_memory(size_t size)
{
    void        *ptr;
	t_heap		*heap;
	t_heap		*last;
	t_heap_type	type;

	type = get_heap_type_from_size(size);
	heap = g_heap[type];
	last = heap;
	while (heap != NULL)
	{
		if (type != LARGE && (ptr = alloc_block(heap, size)))
			return ptr;
		last = heap;
		heap = heap->next;
	}
	heap = create_heap(calc_heap_size(size));
    heap->prev = last;
    if (last)
        last->next = heap;
    else
        g_heap[type] = heap;
    return alloc_block(heap, size);
}

