#include "malloc.h"

void            free(void *ptr)
{
    t_block     *block;
    t_heap      *heap;
    t_heap_type type;

    if (ptr == NULL)
        return ;
    heap = ptr - sizeof(t_block) - sizeof(t_heap);
    block = ptr - sizeof(t_block);
    type = get_heap_type_from_size(block->size);
    if (type == LARGE)
    {
        destroy_heap(heap, LARGE);
        return;
    }
    block->free = 1;
    if (block->next && block->next->free)
        merge_blocks(block, block->next);
    if (block->prev && block->prev->free)
        merge_blocks(block->prev, block);
    if (block->next == NULL && block->prev == NULL)
        destroy_heap(heap, type);
}