/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   debug.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdelory <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/05 16:24:02 by mdelory           #+#    #+#             */
/*   Updated: 2021/01/05 16:41:36 by mdelory          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

char			*type_to_string(t_heap_type type)
{
    return (type == TINY ? "TINY" : (type == SMALL ? "SMALL" : "LARGE"));
}


int             sort_heap(void *h1, void *h2)
{
    return (h1 < h2);
}

void			print_heap(void *heap)
{
    t_heap      *h;
    t_block     *b;

    h = (t_heap *)heap;
    b = HEAP_CONTENT(h);
    ft_printf("%s : %p\n", type_to_string(get_heap_type_from_size(b->size)), h);
    while(b)
    {
        if (b->free == 0)
            ft_printf("%p - %p : %lu octets\n", b + 1, ((void *)(b + 1)) + b->size, b->size);
        b = b->next;
    }
}

size_t          get_allocated_size(t_heap *heap)
{
    t_block     *ptr;
    size_t      total;

    ptr = HEAP_CONTENT(heap);
    total = 0;
    while (ptr)
    {
        if (ptr->free == 0)
            total += ptr->size;
        ptr = ptr->next;
    }
    return total;
}

void            show_alloc_mem()
{
    t_vector    vec;
    t_heap      *h;
    size_t      total;

    vector_init(&vec);
    total = 0;
    h = g_heap[TINY];
    while(h)
    {
        total += get_allocated_size(h);
        vector_add(&vec, h);
        h = h->next;
    }
    h = g_heap[SMALL];
    while(h)
    {
        total += get_allocated_size(h);
        vector_add(&vec, h);
        h = h->next;
    }
    h = g_heap[LARGE];
    while(h)
    {
        total += get_allocated_size(h);
        vector_add(&vec, h);
        h = h->next;
    }
    vector_sort(&vec, sort_heap);
    vector_for_each(&vec, print_heap);
    ft_printf("Total: %lu octets\n", total);
    vector_destroy(&vec);
}

