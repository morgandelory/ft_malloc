#include "malloc.h"

void                *calloc(size_t nmemb, size_t size)
{
    void            *ptr;

    if ((ptr = malloc(nmemb * size)))
        ft_memset(ptr, 0, nmemb * size);
    return ptr;
}