/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdelory <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/04 12:46:34 by mdelory           #+#    #+#             */
/*   Updated: 2021/01/05 17:35:37 by mdelory          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MALLOC_H
# define MALLOC_H

# include <stdlib.h>
# include <unistd.h>
# include <sys/mman.h>

# include "libft.h"

# define TINY_HEAP_ALLOCATION_SIZE (size_t)(4 * getpagesize())
# define TINY_BLOCK_SIZE (size_t)(TINY_HEAP_ALLOCATION_SIZE / 128)
# define SMALL_HEAP_ALLOCATION_SIZE (32 * getpagesize())
# define SMALL_BLOCK_SIZE (size_t)(SMALL_HEAP_ALLOCATION_SIZE / 128)

# define HEAP_CONTENT(x) ((void *)x + sizeof(t_heap))
# define BLOCK_CONTENT(x) ((void *)x + sizeof(t_block))

typedef enum		e_heap_type {
	TINY,
	SMALL,
	LARGE
}					t_heap_type;

typedef struct		s_heap {
	struct s_heap	*prev;
	struct s_heap	*next;
	size_t			size;
    void            *padding;
}					t_heap;

typedef struct		s_block {
	struct s_block	*prev;
	struct s_block	*next;
	size_t			size;
	unsigned int    free;
}					t_block;

t_heap				*g_heap[3];

void				free(void *ptr);
void				*malloc(size_t size);
void                *calloc(size_t nmemb, size_t size);
void				*realloc(void *ptr, size_t size);

void				*alloc_memory(size_t size);

t_heap_type		    get_heap_type_from_size(size_t size);
t_block			    *split_block(t_block *block, size_t size);
void			    *alloc_block(t_heap *heap, size_t size);
void                merge_blocks(t_block *block1, t_block *block2);

size_t              align(size_t size);

void                show_alloc_mem(void);
void			    destroy_heap(t_heap *heap, t_heap_type type);

#endif

