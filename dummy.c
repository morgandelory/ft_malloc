/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdelory <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/05 13:39:15 by mdelory           #+#    #+#             */
/*   Updated: 2021/01/05 17:17:21 by mdelory          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"
#include <errno.h>

#define NALLOC 5000

static int breakit() {
    return 0;
}

void       test_1() {
    void *ptr[NALLOC];
    int j;
    int i;

    for (j = 1; j < NALLOC; j += 3) {
        for (i = 1; i < NALLOC; i += 3) {
            ptr[i] = malloc(j);
//            ft_memset(ptr[i], 'A', j);
        }

        for (i = 1; i < NALLOC; i += 3) {
            free(ptr[i]);
        }
    }
}

void        test_2() {
    void *ptr[NALLOC];
    int i;

    for (i = 1; i < NALLOC; i += 3) {
        ptr[i] = malloc(i);
        ft_memset(ptr[i], i % 10, i);
    }

    show_alloc_mem();

    for (i = 1; i < NALLOC; i += 3) {
        free(ptr[i]);
    }
}

int         main(int ac, char **av) {
    test_1();
    test_2();

    show_alloc_mem();

    return breakit();
}
